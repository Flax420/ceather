# Ceather
A web server for getting weather in terminal or as a status bar

# (Planned) Features
* API from SMHI (perhaps others down the line)
* Single day and 10 day forecasts
* Choice between emojis or text for indicating weather
* Minimal output meaning it's easier to process the data (as opposed to wttr.in/wego which is more difficult to parse)
* Sorting options like highest and lowest temperature, percipitation chance, humidity etc or hourly forecasts
* Helps screen
* JSON or URL arguments

